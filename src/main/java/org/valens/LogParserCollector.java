/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.valens;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

public class LogParserCollector implements TestReportCollector {

    private String errormask = null;
    private String nonerrormask = null;
    private String linemask = null;
    private String suite = null;
    private String linesplittermask = null;
    private String filefound = null;
    private Integer tokennumber = 0;
    final Logger log = Logger.getLogger(TestCollationService.class);

    private LogParserCollector() {

    }

    public LogParserCollector(String filefound, String suite, String errormask, String nonerrormask, String linemask, String linesplittermask, Integer tokennumber) {
        super();
        this.suite = suite;
        this.filefound = filefound;
        this.errormask = errormask;
        this.nonerrormask = nonerrormask;
        this.linemask = linemask;
        this.linesplittermask = linesplittermask;
        this.tokennumber = tokennumber;
    }

    public TestCollectionResult collect(File file) throws Exception {
        TestCollectionResultBuilder builder = new TestCollectionResultBuilder();

        Collection<TestResults> successfulTestResults = Lists.newArrayList();
        Collection<TestResults> failingTestResults = Lists.newArrayList();

        List<String> lines = Files.readLines(file, Charset.forName("UTF-8"));

        int k = 0;
        if(filefound==null || filefound.equalsIgnoreCase("true")){
            TestResults testResults1 = new TestResults("Test Files", file.getName(), "1.0");
            testResults1.setState(TestState.SUCCESS);
            successfulTestResults.add(testResults1);
        }
        
        for (String line : lines) {

            if (!line.matches(linemask)) {
                continue;
            }

            k++;

            String suiteName = "Test Suite " + file.getAbsolutePath();
            if(suite != null && suite.trim().length() > 0)
                suiteName=suite;
            
            String testName = "";
            try {
                testName = line.split(linesplittermask)[tokennumber];
            } catch (Exception e) {
                testName = file.getName() + " test number " + k;
            }           
            
            Double duration = 1.0;
            TestResults testResults = new TestResults(suiteName, testName, duration.toString());
            if (line.matches(errormask) && !line.matches(nonerrormask)) {
                testResults.setState(TestState.FAILED);

                failingTestResults.add(testResults);
            } else {
                testResults.setState(TestState.SUCCESS);
                successfulTestResults.add(testResults);
            }
        }

        return builder
                .addSuccessfulTestResults(successfulTestResults)
                .addFailedTestResults(failingTestResults)
                .build();
    }

    public Set<String> getSupportedFileExtensions() {
        Set<String> s = Sets.newHashSet("log");
        s.add("LOG");

        return s;
    }
}
